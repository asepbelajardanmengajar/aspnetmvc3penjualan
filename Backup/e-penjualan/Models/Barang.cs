﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace e_penjualan.Models
{
    public class Barang
    {
        [Required]
        public int id { get; set; }
        [Required]
        public string code { get; set; }
        [Required]
        public virtual Kategori kategori { get; set; }
        public string nama { get; set; }
        public string keterangan { get; set; }
    }
}