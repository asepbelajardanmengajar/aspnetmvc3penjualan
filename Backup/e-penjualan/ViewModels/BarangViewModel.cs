﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using e_penjualan.Models;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace e_penjualan.ViewModels
{
    public class BarangViewModel : Barang
    {
        PenjualanContext db = new PenjualanContext();
        public IEnumerable<SelectListItem> kategories { get; set; }

        public BarangViewModel()
        {            
            this.kategori = new Kategori();
            this.fillKAtegories();
        }

        public void fillKAtegories()
        {
            this.kategories = db.Kategories.ToList()
                .Select(s => new SelectListItem
                {
                    Value = s.id.ToString(),
                    Text = s.code + "-" + s.nama,
                    Selected = s.id.Equals(this.kategori.id)
                }).ToList();
        }
    }
}