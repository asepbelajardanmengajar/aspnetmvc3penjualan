﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using e_penjualan.Models;
using e_penjualan.ViewModels;
using System.Data.Entity;

namespace e_penjualan.Controllers
{
    public class BarangController : Controller
    {
        PenjualanContext db = new PenjualanContext();
        //
        // GET: /Barang/

        public ActionResult Index()
        {
            var result = db.Barangs.Include("kategori").ToList();
            return View(result);
        }

        //
        // GET: /Barang/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Barang/Create

        public ActionResult Create()
        {
            BarangViewModel barangvm = new BarangViewModel();
            return View(barangvm);
        } 

        //
        // POST: /Barang/Create

        [HttpPost]
        public ActionResult Create(BarangViewModel barangvm)
        {
            if (TryValidateModel(barangvm))
            {
                Barang barang = barangvm;
                barang.kategori = db.Kategories.Find(barang.kategori.id);
                db.Barangs.Add(barang);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(barangvm);
        }
        
        //
        // GET: /Barang/Edit/5
 
        public ActionResult Edit(int? id)
        {
            BarangViewModel barangvm = new BarangViewModel();
            barangvm = (BarangViewModel)db.Barangs.Include("kategori").First(f => f.id==id);
            barangvm.fillKAtegories();
            return View(barangvm);
        }

        //
        // POST: /Barang/Edit/5

        [HttpPost]
        public ActionResult Edit(BarangViewModel barangvm)
        {
            if (TryValidateModel(barangvm))
            {
                Barang barang = db.Barangs.Find(barangvm.id);
                barang.kategori = db.Kategories.Find(barangvm.kategori.id);
                barang.nama = barangvm.nama;
                barang.keterangan = barangvm.keterangan;
                db.Entry(barang).State = EntityState.Modified;                
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(barangvm);
        }

        //
        // GET: /Barang/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Barang/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
