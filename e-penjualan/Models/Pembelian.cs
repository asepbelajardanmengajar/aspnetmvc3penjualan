﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace e_penjualan.Models
{
    public class Pembelian
    {
        [Required]
        public int id { get; set; }
        [Required]
        public Barang Barang { get; set; }
        public double harga { get; set; }
        public int qty { get; set; }
        public string user { get; set; }
    }
}