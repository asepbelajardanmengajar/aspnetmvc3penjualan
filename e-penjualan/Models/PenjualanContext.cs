﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace e_penjualan.Models
{
    public class PenjualanContext:DbContext
    {
        public PenjualanContext()
            : base("name=PenjualanContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
            //Database.SetInitializer<PenjualanContext>(new CreateDatabaseIfNotExists<PenjualanContext>());
            Database.SetInitializer<PenjualanContext>(new DropCreateDatabaseIfModelChanges<PenjualanContext>());
        }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();            
        }

        public DbSet<Kategori> Kategories { get; set; }
        public DbSet<Barang> Barangs { get; set; }
        public DbSet<Pembelian> Pembelians { get; set; }
    }
}