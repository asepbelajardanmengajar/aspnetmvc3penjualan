﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace e_penjualan.Models
{
    public class Barang
    {
        [Required]
        public int id { get; set; }
        [Required]
        public string code { get; set; }
        [Required]
        public Kategori kategori { get; set; }
        public string nama { get; set; }
        public double harga { get; set; }
        public string keterangan { get; set; }
        public string user_id { get; set; }
        [Required]
        public int stok { get; set; }
    }
}