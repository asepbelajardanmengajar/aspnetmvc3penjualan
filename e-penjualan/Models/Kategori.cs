﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace e_penjualan.Models
{
    public class Kategori
    {
        [Required]
        public int id { get; set; }        
        public string code { get; set; }
        public string nama { get; set; }
        public string keterangan { get; set; }
    }
}