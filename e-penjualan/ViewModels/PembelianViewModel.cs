﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_penjualan.ViewModels
{
    public class PembelianViewModel
    {
        public int idBarang { get; set; }
        public int idKategori { get; set; }
        public string KodeBarang { get; set; }
        public string NamaBarang { get; set; }
        public double harga { get; set; }
        public int qty { get; set; }
    }
}