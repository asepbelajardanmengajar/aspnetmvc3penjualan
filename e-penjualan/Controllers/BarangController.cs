﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using e_penjualan.Models;
using e_penjualan.ViewModels;
using System.Data.Entity;

namespace e_penjualan.Controllers
{
    public class BarangController : Controller
    {
        PenjualanContext db = new PenjualanContext();
        //
        // GET: /Barang/
        [Authorize(Roles = "admin")]
        public ActionResult Index()
        {
            var result = db.Barangs.Include("kategori").ToList();
            return View(result);
        }

        [Authorize(Roles = "memberstore")]
        public ActionResult IndexMembers()
        {
            var result = db.Barangs.Include("kategori").ToList();
            return View(result);
        }

        //
        // GET: /Barang/Details/5
        [Authorize(Roles = "memberstore,admin")]
        public ActionResult Details(int id)
        {
            Barang barang = db.Barangs.Find(id);
            barang.kategori = db.Kategories.Find(barang.kategori.id);
            return View(barang);
        }        

        //
        // GET: /Barang/Create
        [Authorize(Roles = "admin")]
        public ActionResult Create()
        {
            BarangViewModel barangvm = new BarangViewModel();
            return View(barangvm);
        } 

        //
        // POST: /Barang/Create

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult Create(BarangViewModel barangvm)
        {
            if (TryValidateModel(barangvm))
            {
                Barang barang = barangvm;
                barang.kategori = db.Kategories.Find(barang.kategori.id);
                db.Barangs.Add(barang);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(barangvm);
        }
        
        //
        // GET: /Barang/Edit/5
        [Authorize(Roles = "admin")]
        public ActionResult Edit(int? id)
        {
            BarangViewModel barangvm = new BarangViewModel();
            barangvm = (BarangViewModel)db.Barangs.Include("kategori").First(f => f.id==id);
            barangvm.fillKAtegories();
            return View(barangvm);
        }

        //
        // POST: /Barang/Edit/5

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult Edit(BarangViewModel barangvm)
        {
            if (TryValidateModel(barangvm))
            {
                Barang barang = db.Barangs.Find(barangvm.id);
                barang.kategori = db.Kategories.Find(barangvm.kategori.id);
                barang.nama = barangvm.nama;
                barang.keterangan = barangvm.keterangan;
                db.Entry(barang).State = EntityState.Modified;                
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(barangvm);
        }

        //
        // GET: /Barang/Delete/5

        [Authorize(Roles = "admin")]
        public ActionResult Delete(int id)
        {
            Barang b =db.Barangs.Find(id);
            return View(b);
        }

        //
        // POST: /Barang/Delete/5

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
