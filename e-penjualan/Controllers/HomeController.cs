﻿using e_penjualan.Models;
using e_penjualan.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace e_penjualan.Controllers
{
    public class HomeController : Controller
    {
        PenjualanContext db = new PenjualanContext();
        public ActionResult Index()
        {
            var result = db.Barangs.Include("kategori").ToList()
                .Select( x => new PembelianViewModel{
                            idBarang= x.id,
                            idKategori = x.kategori.id,
                            KodeBarang =  x.code, 
                            NamaBarang = x.kategori.code+ "-" + x.nama,
                            qty = x.stok ,
                            harga = x.harga 
                        }).ToList();

            ViewBag.Message = "Welcome to ASP.NET MVC!";

            return View(result);
        }

        [Authorize(Roles = "memberstore,admin")]
        public ActionResult Details(int id)
        {
            var result = db.Barangs.Include("kategori").ToList()
                .Select(x => new PembelianViewModel
                {
                    idBarang = x.id,
                    idKategori = x.kategori.id,
                    KodeBarang = x.code,
                    NamaBarang = x.kategori.code + "-" + x.nama,
                    qty = 0,
                    harga = x.harga
                }).ToList().Where(w=> w.idBarang== id).FirstOrDefault();
            return View(result);
        }

        [HttpPost]
        public ActionResult Details(PembelianViewModel pmb)
        {
            //simpan pembelian
            var barang = db.Barangs.Include("kategori")
                .ToList()
                    .Where(w => w.id == pmb.idBarang)
                        .FirstOrDefault();
            Pembelian pembelian = new Pembelian();
            pembelian.Barang = barang;
            pembelian.harga = barang.harga;
            pembelian.qty = pmb.qty;
            //ambil username yang beli
            pembelian.user = Membership.GetUser().UserName;
            db.Entry(pembelian).State = EntityState.Added;
            db.SaveChanges();
            //update barang
            Barang barangtmp = db.Barangs.Include("kategori").First(f => f.id == pembelian.Barang.id);
            barangtmp.stok = (barangtmp.stok - pembelian.qty);
            db.Entry(barangtmp).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");
        }
        public ActionResult About()
        {
            return View();
        }

        [Authorize(Roles = "memberstore,admin")]
        public ActionResult Pembelian()
        {
            string roleName = Roles.GetRolesForUser().ToArray()[0];//ambil roles yang pertama, karena setiap user disini hanya 1 role
            
            var pembelians = db.Pembelians.Include("barang").ToList();

            //kalo bukan role admin, hanya tampilkan pembelian usertersebut saja
            if(!roleName.Equals("admin")){
                String username = Membership.GetUser().UserName;
                pembelians = pembelians.AsQueryable().Where(w => w.user.Equals(username)).ToList();
            }

            var result = pembelians.Select(x => new PembelianViewModel
            {
                idBarang = x.id,
                idKategori = x.Barang.kategori.id,
                KodeBarang = x.Barang.code,
                NamaBarang = x.Barang.code + "-" + x.Barang.nama,
                qty = x.qty,
                harga = x.harga
            }).ToList();
            
                
            return View(result);
        }
    }
}
