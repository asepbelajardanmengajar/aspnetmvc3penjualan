﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using e_penjualan.Models;
using System.Data.Entity;

namespace e_penjualan.Controllers
{
    public class KategoriController : Controller
    {
        PenjualanContext db = new PenjualanContext();
        //
        // GET: /Kategori/
        [Authorize(Roles = "admin")]
        public ActionResult Index()
        {
           List<Kategori> dt =  db.Kategories.ToList();
            return View(dt);
        }

        //
        // GET: /Kategori/Details/5
        [Authorize(Roles = "admin")]
        public ActionResult Details(int? id)
        {
            Kategori kategori = db.Kategories.Find(id);
            return View(kategori);
        }

        //
        // GET: /Kategori/Create
        [Authorize(Roles = "admin")]
        public ActionResult Create()
        {
            return View(new Kategori());
        } 

        //
        // POST: /Kategori/Create

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult Create(Kategori kategori)
        {
            if(ModelState.IsValid ){
                db.Kategories.Add(kategori);
                db.SaveChanges();
            }
            return Redirect("index");
        }
        
        //
        // GET: /Kategori/Edit/5
        [Authorize(Roles = "admin")]
        public ActionResult Edit(int? id)
        {
            Kategori kategori = db.Kategories.Find(id);
            return View(kategori);
        }

        //
        // POST: /Kategori/Edit/5

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult Edit(Kategori kategori)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kategori).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kategori);
        }

        //
        // GET: /Kategori/Delete/5

        [Authorize(Roles = "admin")]
        public ActionResult Delete(int? id)
        {
            Kategori kategori = db.Kategories.Find(id);
            return View(kategori);
        }

        //
        // POST: /Kategori/Delete/5

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult Delete(Kategori kategori)
        {
            kategori = db.Kategories.Find(kategori.id);
            db.Kategories.Remove(kategori);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
